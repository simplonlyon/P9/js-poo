let form = document.querySelector("form");
let playGround = document.querySelector("#playground");
let tab = [];
let selected = null;

form.addEventListener("submit", function (event) {
    event.preventDefault();

    /*Soit on récupère les valeurs avec un objet FormData (qui attend
    un formulaire HTML en argument et se base sur les name des inputs/select pour choper les données)
    puis on utilise ce FormData pour créer notre instance
    */
    // let data = new FormData(form);
    // let shapeCreated = new Shape(data.get('width'), 
    //                             data.get('height'), 
    //                             data.get('color'), 
    //                             data.get('roundness'),
    //                             30, 
    //                             30);
    /* Soit on crée l'instance de Shape en récupérant directement les valeurs
    du formulaire sans passer par des variables intérmédiaires
    */
    // let shapeCreated = new Shape(document.querySelector("#width").value,                                               
    //     document.querySelector("#height").value,
    //     document.querySelector("#color").value, 
    //     document.querySelector("#roundness").value, 
    //     30, 
    //     30);
    /* Soit on crée un objet/des variables dans le/lesquelles on stock
    les valeurs des inputs/select du formulaire puis on se sert de cet
    objet/ces variables pour créer l'instance de Shape 
    */
    let valuesCreated = {
        width: document.querySelector("#width").value,
        height: document.querySelector("#height").value,
        color: document.querySelector("#color").value,
        roundness: document.querySelector("#roundness").value,
    };
    // console.log(valuesCreated);
    if (selected === null) {
        let shapeCreated = new Shape(valuesCreated.width,
            valuesCreated.height,
            valuesCreated.color,
            valuesCreated.roundness,
            Math.random() * 1000,
            Math.random() * 100);

        tab.push(shapeCreated);

        let stockDraw = shapeCreated.draw();
        stockDraw.addEventListener('click', function () {
            selected = shapeCreated;
            changeForm(selected);
        });
        playGround.appendChild(stockDraw);

    } else {
        selected.width = valuesCreated.width;
        selected.height = valuesCreated.height;
        selected.color = valuesCreated.color;
        selected.roundness = valuesCreated.roundness;
        selected.draw();
        // playGround.innerHTML = '';
        // for (const item of tab) {
        //     let stockDraw = item.draw();
        //     stockDraw.addEventListener('click', function () {
        //         selected = item;
        //         changeForm(item);
        //     });
        //     playGround.appendChild(stockDraw);
        // }

    }
});

/**
 * @param {Shape} shape 
 */
function changeForm(shape) {
    document.querySelector('#width').value = shape.width;
    document.querySelector('#height').value = shape.height;
    document.querySelector('#color').value = shape.color;
    document.querySelector('#roundness').value = shape.roundness;

}