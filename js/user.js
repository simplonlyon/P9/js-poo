

class User {
    /**
     * @param {string} paramUsername 
     * @param {string} paramPassword 
     * @param {string} paramEmail 
     */
    constructor(paramUsername, paramPassword, paramEmail) {
        this.username = paramUsername;
        this.password = paramPassword;
        this.email = paramEmail;
    }
    /*
     * On peut accéder aux propriétés de l'instance qui déclenche la 
     * méthode en utilisant this.propriété à l'intérieur de celle ci
     */
    /**
     * Méthode qui présente l'utilisateur.ice
     * @returns {string}
     */
    greetings() {
        return `Hello my name is ${this.username} you can
         contact at ${this.email}`;
    }
    /**
     * 
     * @param {string} username 
     * @param {string} password 
     * @returns {boolean}
     */
    login(username, password) {
        if(username === this.username && password === this.password) {
            return true;
        }
        return false;

    }

}