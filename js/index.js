
/**
 * Pour créer une instance, on utilise le mot clef new suivi du nom
 * de la classe à instancier, les argument qu'on donne à l'intérieur
 * des parenthèses correspondent à ceux définis par la méthode constructor
 */
let instanceDog = new Dog('fido', 'corgi', 1);
console.log(instanceDog);
/*
instanceof permet de vérifier si le contenu d'une variable est une
instance d'une classe
*/
console.log(instanceDog instanceof Dog);
instanceDog.bark();

/*
A noter que si je fais un objet inline et que je le fais ressembler
à un Dog, ça n'en est pas un pour de vrai. Ainsi, le instanceof
me renverra false si je lui demande s'il est une instance de Dog
*/
// let dog = {name: 'fido', breed:'corgi', age: 1};
// console.log(dog);
// console.log(dog instanceof Dog);
