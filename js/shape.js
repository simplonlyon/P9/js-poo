class Shape {
    /**
     * @param {number} width 
     * @param {number} height 
     * @param {string} color 
     * @param {number} roundness 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(width, height, color, roundness, x, y) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.roundness = roundness;
        this.x = x;
        this.y = y;
        this.element = null;
    }
    /**
     * Une méthode qui génère un élément HTML correspondant à l'instance
     * ayant déclenchée la méthode
     * @returns {HTMLDivElement} L'élément HTML représentant l'instance
     */
    draw() {
        let div;
        if(this.element !== null) {
            div = this.element;
        }else {
            div = document.createElement("div");
            this.element = div;
        }
        div.style.width = this.width + "px";
        div.style.height = this.height + "px";
        div.style.backgroundColor = this.color;
        div.style.borderRadius = this.roundness + "px";
        div.style.left = this.x + "px";
        div.style.top = this.y + "px";
        div.style.position = "absolute";
        return div;
    }
    
}