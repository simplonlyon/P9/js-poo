/*
1. Créer un nouveau fichier user.js
2. Dans ce fichier, créer une classe User avec comme propriétés un username, un password et un email. Faire le constructor correspondant
3. Faire une méthode (fonction) login dans la classe User qui attendra un username et un password en argument
4. Faire que si les arguments donnés correspondent aux valeurs des propriétés username/password de l'instance, ça renvoie true, sinon ça renvoie false
5. Dans le fichier exo-class.js, créer deux instances de user et faire appel à leur méthode login, une fois avec des bonnes valeurs, une fois avec des mauvaises
6. Faire une méthode greetings() dans le classe User qui va return une chaîne de caractère avec "Hello, my name is .... you can contact me at ..." en remplissant les trous par le username et l'email de l'instance

*/

let user = new User('Autre', '1234', 'jean@mail.com');
console.log(user);

console.log(user.greetings());
console.log(user.login('Autre', '1234'));

user.login()
let user2 = new User('Bloup', '123', 'autre@autre.com');

console.log(user2.greetings());

